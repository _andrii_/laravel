<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{

    public static function index($city)
    {
        $ch = curl_init('https://api.openweathermap.org/data/2.5/weather?q='. $city .'&appid=466c5e5450b024efe9e76c72fa2df5f5&mode=html');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        curl_close($ch);

        return view("weather.index", ['weather' => $html]);
    }

    public function selectForm()
    {
        return view("weather.form");

    }
    public function selForm(Request $request)
    {
        $city = $request->input('city');
        return WeatherController::index($city);
    }

}
