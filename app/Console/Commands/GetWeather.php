<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RakibDevs\Weather\Weather;

class GetWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show weather info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $wt = new Weather();

        $info = $wt->getCurrentByCity('Ivano-Frankivsk');
        var_dump($info);
        //return 0;
    }
}
