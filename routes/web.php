<?php

use App\Http\Controllers\WeatherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('weather')->group(function () {
    Route::get('/index/{city}', [WeatherController::class, 'index'])->name('weather');
    Route::get('/show', [WeatherController::class, 'selectForm'])->name('weather.select');
    Route::post('/show', [WeatherController::class, 'selForm'])->name('weather.show');
});

